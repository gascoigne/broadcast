package broadcast_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gascoigne/broadcast"
)

func TestBroadcast(t *testing.T) {
	type subFn func(g *broadcast.Group) *broadcast.Subscription

	unbufferedSub := func() subFn {
		return func(g *broadcast.Group) *broadcast.Subscription {
			return g.Subscribe()
		}
	}

	bufferedSub := func(buffer int) subFn {
		return func(g *broadcast.Group) *broadcast.Subscription {
			return g.SubscribeBuffered(buffer)
		}
	}

	type tc struct {
		subs  []subFn
		nmsgs int
	}

	tcName := func(tc tc, i int) string {
		return fmt.Sprintf("%v_%v_%v", i, len(tc.subs), tc.nmsgs)
	}

	tcs := make([]tc, 0)

	makeTcs := func(fn func(i int) subFn) {
		for i := 0; i < 10; i++ {
			nmsgs := 1 << i
			for j := 0; j < 10; j++ {
				nsubs := 1 << j

				tc := tc{
					nmsgs: nmsgs,
					subs:  make([]subFn, nsubs),
				}
				for k := 0; k < nsubs; k++ {
					tc.subs[k] = fn(k)
				}
				tcs = append(tcs, tc)
			}
		}
	}

	makeTcs(func(i int) subFn {
		return unbufferedSub()
	})

	makeTcs(func(i int) subFn {
		return bufferedSub(i)
	})

	makeTcs(func(i int) subFn {
		if i%2 == 0 {
			return bufferedSub(i)
		} else {
			return unbufferedSub()
		}
	})

	for i, tc := range tcs {
		t.Run(tcName(tc, i), func(t *testing.T) {
			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Minute)
			t.Cleanup(cancel)

			g := broadcast.NewGroup()
			go g.Run(ctx)

			subs := make([]*broadcast.Subscription, len(tc.subs))
			for i, fn := range tc.subs {
				subs[i] = fn(g)
				t.Cleanup(subs[i].Close)
			}

			for msg := 0; msg < tc.nmsgs; msg++ {
				err := g.Send(ctx, msg)
				assert.NoError(t, err, "sending")

				for i, sub := range subs {
					rmsg, err := sub.Recv(ctx)
					assert.NoError(t, err, "receiving on sub %v", i)
					assert.Equal(t, msg, rmsg, "receiving on sub %v", i)
				}
			}
		})
	}
}
