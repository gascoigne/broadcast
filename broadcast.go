// Package broadcast provides a one-to-many communication channel
package broadcast

import (
	"context"
	"sync"

	"github.com/google/uuid"
)

// SubscriberID is the unique id of a given subscription
type SubscriberID string

// A Group is a one-to-many communication channel.
//
// A Group, once initialized, must be started via the Run() method.
//
// Interested parties may Subscribe to the group. Values sent into the
// group will be asynchronously broadcast to all subscribers.
type Group struct {
	m    sync.RWMutex
	in   chan interface{}
	outs map[SubscriberID]chan<- interface{}
}

// NewGroup initializes a new group.
func NewGroup() *Group {
	return &Group{
		in:   make(chan interface{}),
		outs: make(map[SubscriberID]chan<- interface{}),
	}
}

// Send broadcasts a value to all subscribers.
func (g *Group) Send(ctx context.Context, msg interface{}) error {
	g.m.RLock()
	defer g.m.RUnlock()

	select {
	case <-ctx.Done():
		return ctx.Err()
	case g.in <- msg:
		return nil
	}
}

// Run pumps messages through the group. It is a blocking call.
func (g *Group) Run(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		case msg := <-g.in:
			g.send(ctx, msg)
		}
	}
}

func (g *Group) send(ctx context.Context, msg interface{}) {
	g.m.RLock()
	defer g.m.RUnlock()

	for i := range g.outs {
		out := g.outs[i]
		go func(out chan<- interface{}) {
			select {
			case <-ctx.Done():
			case out <- msg:
			}
		}(out)
	}
}

// Subscribe creates a new subscription which can receive values.
//
// Subscriptions must be cleaned up when the subscription is no longer
// in use, via the Close method.
func (g *Group) Subscribe() *Subscription {
	c := make(chan interface{})
	return g.subscribeWithChan(c)
}

// SubscribeBuffered creates a new subscription using a buffered
// channel.
//
// In situations where you know how many in-flight values you expect
// to have, using a buffer can reduce the number of goroutines that
// are blocked waiting to send values to subscribers.
func (g *Group) SubscribeBuffered(buffer int) *Subscription {
	c := make(chan interface{}, buffer)
	return g.subscribeWithChan(c)
}

func (g *Group) subscribeWithChan(c chan interface{}) *Subscription {
	sub := newSubscription(g, c)
	g.addSubscriber(sub.ID, c)
	return sub
}

func (g *Group) addSubscriber(id SubscriberID, c chan<- interface{}) {
	g.m.Lock()
	defer g.m.Unlock()

	g.outs[id] = c
}

func (g *Group) removeSubscriber(id SubscriberID) {
	g.m.Lock()
	defer g.m.Unlock()

	if c, ok := g.outs[id]; ok {
		delete(g.outs, id)
		close(c)
	}
}

// A Subscription represents a subscription to a Group.
type Subscription struct {
	ID SubscriberID
	g  *Group
	c  <-chan interface{}
}

func newSubscription(g *Group, c <-chan interface{}) *Subscription {
	return &Subscription{
		ID: SubscriberID(uuid.NewString()),
		g:  g,
		c:  c,
	}
}

// Close will remove this subscription from the group and clean up any
// associated resources.
func (s *Subscription) Close() {
	s.g.removeSubscriber(s.ID)
}

// Chan returns a channel on which values for this subscription will
// be sent.
func (s *Subscription) Chan() <-chan interface{} {
	return s.c
}

// Recv is a blocking receive on a value from this subscription.
func (s *Subscription) Recv(ctx context.Context) (interface{}, error) {
	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	case msg := <-s.c:
		return msg, nil
	}
}
